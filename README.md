
[![Pipeline Status](https://gitlab.com/geogirafe/lighttpd/badges/main/pipeline.svg)](https://gitlab.com/geogirafe/lighttpd/-/pipelines)
[![Docker Pulls](https://img.shields.io/docker/pulls/geogirafe/lighttpd.svg)](https://hub.docker.com/r/geogirafe/lighttpd/)

This is just a clean install of lighttpd on Alpine Linux.  

# How to build

```
docker build -t geogirafe/lighttpd .
```

# How to run

### Simple

```
docker run -d --name lighttpd -p 8080:80 -p 8443:443 geogirafe/lighttpd
```

### Mount your custom website

```
docker run -d --name lighttpd -v <local-directory>:/var/www/localhost/htdocs -p 8080:80 -p 8443:443 geogirafe/lighttpd
```

### Match the host timezone

```
docker run -d --name lighttpd -v /etc/timezone:/etc/timezone -p 8080:80 -p 8443:443 geogirafe/lighttpd
```

### Use custom configuration

```
docker run -d --name lighttpd -v <local-directory>:/etc/lighttpd -p 8080:80 -p 8443:443 geogirafe/lighttpd
```
